// on gitbash terminal, input npm init hit enter until asked if is this ok?, enter yes
// Shortcut: npm init -y

// Once package is isntalled, type npm install express

// Use the "require" directive to load the express module/package
// A "module" is a software component or part of a program that contains one or more routines.
	// "express" is a module
// This is used to get the contents of the express package to be used by our application.
// It also allows us to access methods and functions that will allow us to easily create a server
const express = require("express");

// Create an application using express
// This creates an express application and stores this in a constant called app
// In layman's terms, app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 4000;

// Setup for allowing the server to handle data from requests
// Allows your app to read JSON data
// Methods used from express JS are middlewares
// Middleware is software that provides common services and capabilities to applications outside of what's offered by the operating system
// API management is one of the common applications of middlewares
app.use(express.json());

// Allows your app to read data from forms
// By default, information received as a string or an array
// By applying the option of "extended: true", this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// [SECTION] Routes
// Express has methods corresponding to each HTTP method
// This route exprects to receive a GET request at the base URI of "/"
// The full base URI for our local application for this route will be at "http://localhost:4000"
// This route will return a simple message back to the client
app.get("/", (req, res) => {
	// Once the route is accessed, it will send a string response containing "Hello, world"
	// Compared to our previous session, res.send uses the node JS modules' method
	// res.send uses the express JS module's method instead to send a response back to the client.
	res.send("Hello, world");
});


app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint");
});

app.post("/hello", (req, res) => {
	// req.body contains the contents/data of the request body
	// All the properties defined in our Postman request will be accessible here as properties with the same name
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
});

// Declaration of mock database
// An array that will store user objects when the "/signup route is accessed"
let users = [];

// This route expects to receive a POST request at the URI "/signup"
// This will create a user objct in the "users" variable that mirrors a real world registration process
app.post("/signup", (req, res) => {
	console.log(req.body);

	// Actual request boolean
	// If contents of the "request body" with the property "username" and "password" is not empty
	if(req.body.username !== '' && req.body.password !== ''){
		// This will store the user object sent via Postman to the users array created above
		users.push(req.body);

		res.send(`User ${req.body.username} successfully reqistered`);
	} else {
		res.send("Please input BOTH username and password");
	}
});

app.put("/change-password", (req, res) => {
	// Create a variable to store the message to be sent back to the client/ postman
	let message;

	// Create a for loop that will loop through the elements of the users array
	for(let i = 0; i<users.length; i++){
		// If the username provided in the client/ postman and the username of the current object in the loop is the same
		if (req.body.username == users[i].username) {
			
			// Changes the password of the user found by the loop into the password provided in the client/postman
			users[i].password = req.body.password;

			// Changes the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated.`

			console.log(users);

			// breaks out of the loop once a user matches the username provided in the client/postman is found
			break;
		}
		// If no user is found
		else {
			// Changes the message to be sent back by the response
			message = "User does not exist.";
		}
	}

	res.send(message);
})


// Tells our server to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal

// if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly
// else, if it is needed to be imported, it will not run the app and instead exportit to be used in another file.
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port: ${port}`));
}

module.exports = app;