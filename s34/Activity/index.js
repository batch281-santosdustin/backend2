// Create several routes using REST API and Express JS methods to perform different tasks

// Access Express module
const express = require("express");

// Creates an application using express
const app = express();

const port = 3000;

// Allows the app to read JSON data
app.use(express.json());

// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

// [SECTION] Create a GET route that will access the /home route that will send a response with message: "Welcome to the home page."
app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});

// Declaration of mock database
let users = [];

// [ADD] Inputting users to the database
app.post("/users", (req, res) => {
	// Checks if the username and password is not empty
	if(req.body.username !=='' && req.body.password !=='') {
		users.push(req.body);

		res.send(`Users ${req.body.username} successfully registered`);
	} else {
		res.send("Please input BOTH username and password");
	}

	console.log(users);
});

// [SECTION] GET route that will access the /users route that will send the users' array as a response
app.get("/users", (req, res) => {
	res.send(users);
})

// [SECTION] Create a DELETE route that will access the /delete-user toute to remove a user from the mock database
app.delete("/delete-users", (req, res) => {
	// Create a variable to store the message to be send back to the client/ Postman
	let message;

	// Create a condition to check if there are users found in the array
	if(users.length > 0) {
		// If there are, create a for-loop that will loop through the elements of the "users" array
		for (let i = 0; i < users.length; i++) {
				if(req.body.username == users[i].username){
					// If it is, remove the current object from the array. You can use splice array method
					users.splice(i, 1);

					// Change the message to be sent back by the response: "User <username> has been deleted"
					message = `User ${req.body.username} has been deleted.`;

					// Break the loop
					break;
				}

				// Add an else statement if there are no users in the array, update the message variable with the following message: "No Users found."
				else {
					message = "No users found.";
				}
		}

		// Outside the loop, add an if statement that if the message variable is undefined, update the message variable with the following message: "User does not exist"
		if (message == undefined) {
			message = "User does not exist.";
		} 
	}

	// Send the message variable as a response back to the client/ postman once the user has been deleted or if a user is not found
	res.send(message);
	console.log(users);

})

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port: ${port}`));
}

module.exports = app;