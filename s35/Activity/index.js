const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Allows to read json data
app.use(express.json());

// Allows the app to read data from forms
app.use(express.urlencoded({extended: true}));

// MongoDB Connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://santosdustinaaron07:Ayu2LjfBRn3QDOpQ@wdc028-course-booking.fhfpbwj.mongodb.net/b281_to-do?retryWrites=true&w=majority", 
		{
			// Deprecators: It will give warnings if there are concerns
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
);

// Sets a notification if success or fail, will handle certain error correction
let db = mongoose.connection;

// Displays a statement if a connection is not successful
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// [SECTION] Create a USER SCHEMA
// Mongoose Schemas
const userSchema = new mongoose.Schema({
	username : String,
	password : String
}, { collection : 'user' });

// [SECTION] Create a USER MODEL
// Models
const User = mongoose.model("User", userSchema);

// [SECTION] Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup", (req, res) => {

	User.findOne({ username : req.body.username}).then((result, err) => {
		if(result != null && result.name == req.body.name){
			return res.send("Username already exists.");
		} else {
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user created");
				}
			})
		}
	})
})

// Get all users
app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));