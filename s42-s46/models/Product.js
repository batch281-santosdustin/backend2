// [SECTION] Dependencies
const mongoose = require('mongoose');

// [SECTION] Schema declaration
const productSchema = new mongoose.Schema({
	sku : {
		type: String,
		required: [true, "SKU is required."]
	},
	businessUnit: {
		type: String,
		enum: ['Technology Solutions', 'Digital Solutions', 'Data Solutions'],
		required: [true, "Business Unit is required."]
	},
	category: {
	  	type: String,
	  	required: [true, "Category is required."],
	  	enum: ['CCTV', 'Supplies', 'Webbiz', 'Buzzboost', 'D-ALL', 'D-CHECK']
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	origPrice: {
		type: Number,
	},
	percentVat: {
		type: Number
	},
	srp: {
		type: Number,
	},
	inventory: {
		type: Number,
		default: 0
	},
	isActive: {
		type: Boolean,
		default: true
	},
	onSale: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: Date()
	}

});


// [SECTION] Export
module.exports = mongoose.model('Product', productSchema);