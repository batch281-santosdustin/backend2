// [SECTION] Dependencies
const Product = require('../models/Product');
const Client = require('../models/Client');
const Order = require('../models/Order');

// [SECTION] Functions
module.exports.addToCart = async (data) => {
  try {
    const client = await Client.findById(data.clientId);
    if (!client) {
      throw new Error("Client not found");
    }

    const newCartItems = [];

    for (const productData of data.updateData) {
      const product = await Product.findById(productData.productId);
      if (!product) {
        throw new Error(`Product with ID ${productData.productId} not found`);
      }

      const subtotal = product.srp * productData.quantity;

      const newOrderItem = {
        productId: productData.productId,
        sku: product.sku,
        quantity: productData.quantity,
        subtotal: subtotal.toFixed(2)
      };

      newCartItems.push(newOrderItem);
    }

    const order = new Order({
      clientId: client._id,
      clientName: client.name,
      products: newCartItems,
      total: calculateCartTotal(newCartItems)
    });

    client.carts.push({
      cartId: order._id,
      products: newCartItems,
      total: order.total,
      createdOn: new Date(),
      checkOut: false
    });

    await Promise.all([order.save(), client.save()]);

    return "Products added to cart successfully";
  } catch (error) {
    console.log(error);
    return "Error adding products to cart";
  }
};

function calculateCartTotal(products) {
  let total = 0;
  for (const product of products) {
    total += parseFloat(product.subtotal);
  }
  return total.toFixed(2);
}

// Update a cart
module.exports.updateCart = async (cartId, updateData) => {
  try {
    const order = await Order.findById(cartId);
    if (!order) {
      return "Cart not found";
    }

    const updatedCartItems = [];

    for (const item of updateData) {
      const product = await Product.findById(item.productId);
      if (!product) {
        return `Product with ID ${item.productId} not found`;
      }

      const subtotal = product.srp * item.quantity;

      const updatedOrderItem = {
        productId: item.productId,
        sku: product.sku,
        quantity: item.quantity,
        subtotal: subtotal.toFixed(2),
      };

      updatedCartItems.push(updatedOrderItem);
    }

    order.products = updatedCartItems;
    order.total = calculateCartTotal(updatedCartItems);

    const client = await Client.findOneAndUpdate(
      { "carts.cartId": order._id },
      {
        $set: {
          "carts.$.products": updatedCartItems,
          "carts.$.total": order.total,
        },
      }
    );

    await Promise.all([order.save(), client.save()]);

    return "Cart updated successfully";
  } catch (error) {
    console.log(error);
    return "Error updating cart";
  }
};

// Checkout a cart
module.exports.checkoutCart = async (cartId) => {
  try {
    const order = await Order.findById(cartId);
    if (!order) {
      return "Cart not found";
    }

    // Update the checkout data
    order.checkOut = true;
    order.checkoutDate = new Date();

    // Save the updated order
    await order.save();

    // Update the client's cart checkout status
    const client = await Client.findOneAndUpdate(
      { "carts.cartId": order._id },
      { $set: { "carts.$.checkOut": true } }
    );

    return "Cart checked out successfully";
  } catch (error) {
    console.log(error);
    return "Error checking out cart";
  }
};

