// [SECTION] Dependencies
const express = require('express');
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

// [SECTION] Routes
// Ordering a product
router.put("/addToCart", auth.verify, (req, res) => {
	const data = {
		clientId: auth.decode(req.headers.authorization).id,
		updateData: req.body
	}

	orderController.addToCart(data)
		.then(resultFromController => res.send(resultFromController));
})

// Update a cart
router.put("/:cartId", auth.verify, (req, res) => {
  const cartId = req.params.cartId;
  const updateData = req.body;
  
  orderController.updateCart(cartId, updateData)
    .then(resultFromController => res.send(resultFromController));
});

// Checkout a cart
router.put("/:cartId/checkout", auth.verify, (req, res) => {
  const cartId = req.params.cartId;

  orderController.checkoutCart(cartId)
    .then(resultFromController => res.send(resultFromController));
});

// Retrieve all carts (admin only)
router.get("/carts", auth.verify, (req, res) => {
	const data = {
		adminId: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		orderController.getCarts(data)
			.then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	}
})

// Retrieve all checkout (admin only)
router.get("/carts/checkout", auth.verify, (req, res) => {
	const data = {
		adminId: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		orderController.getCheckout(data)
			.then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	}
})


// [SECTION] Export
module.exports = router;