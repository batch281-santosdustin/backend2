// Controllers contain the functions and business logic of our express js application
// Meaning, all the operations it can do will be place in this file.

// Uses the "require" directive to allow access to the "Task" model which allows us to access the Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task");

// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" and exports these functions
module.exports.getAllTasks = () => {

	// The return statement, returns the result of the mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/task" routes is accessed
	// "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {
	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({

		// Sets the "name" property with the value received from the client/postman
		name : requestBody.name
	})

	// Saves the newly created "newTask" object in the MongoDB database
	// the "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/postman
	// The "then" method will accept the following 2 arguments
		// The first parameter will store the result returned by the Mongoose "save" method
		// The second parameter will store the "error" of the object
	return newTask.save().then((task, error) => {
		// If an error is encountered returns a "false" boolean back to the client/postman
		if(error) {
			console.log(error);
			// If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
			// Since the following return statement is nested within the "then" method chained to the "save" method and they donot prevent each other from executing code
			// The else statement will no longer be evaluated
			return false;
		} else {
			return task;
		}
	})
}

// Controller function for deleting a task
// The task id retrieved from the "req.params.id" property coming from the client is named as a "taskId" parameter in the controller file
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		// If an error is encountered returns a "false"
		if(err){
			console.log(err);
			return false;
		}

		// Delete successful, returns the removed task object back to the client/ postman
		else {
			return removedTask;
		}
	})
}

// Controller function for updating a task
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask = (taskId, newContent) => {

	// The "findById" mongoose method will look for a task with the same ID provided in the URL
	// "findById" is the same as "find({_id : value})"
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function
	return Task.findById(taskId).then((result, error ) => {

		// if an error is encountered returns a "false" boolean back to the client/Postman
		if(error){
			console.log(error);
			return false;
		} 

		// Results of the "findById" method will be stored in the "result" parameter
		// It's "name" property will be reassigned the value of the "name" received from the request
		result.name = newContent.name;

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter
		// Because of mongoose we have access to the save method to update existing document with the changes we applied
		return result.save().then((updateTask, saveErr) => {

			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})

	})
}

// [SECTION: ACTIVITY] Create a controller function for retrieving a specific task
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

// [SECTION: ACTIVITY] Create a controller function for changing the status of a task to complete.
module.exports.updateStatus = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.status = "complete";

		return result.save().then((updateStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateStatus;
			}
		})
	})
}

// [SECTION: TRY] Create a controller for changing hte status of a task that is dependent on the /:id/:status
/*module.exports.updateStatus = (taskId, newStatus) => {
  return Task.findByIdAndUpdate(taskId, { status: newStatus }, { new: true })
    .exec();
};*/