// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://santosdustinaaron07:Ayu2LjfBRn3QDOpQ@wdc028-course-booking.fhfpbwj.mongodb.net/b281_to-do?retryWrites=true&w=majority", 
		{
			// Deprecators: It will give warnings if there are concerns
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
);

// Add the task route
// Allows all the task routes created in the "taskRoute.js file to use "/tasks" routes
// Server listening
app.use("/tasks", taskRoute)

// http:/localhost:${port}/tasks

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port: ${port}`));
}

module.exports = app;